#!/usr/bin/python3

import os
from time import sleep
import random

from tweeds.utils.constants import (
    FILE_PATH_CREDENTIALS,
    FILE_PATH_RECORDS,
    FILE_PATH_COUNT,
    URL_TWEET,
)
from tweeds.utils.helpers import (
    ConfigHelpers,
    read_record,
    write_record,
)
from tweeds.utils.twitter import Twitter, TwitterDriver

CHAIRMAN_TWITTER = 'ImranKhanPTI'
CATCH_WORDS = [
    'shut up',
    'bonga',
    'baunga',
    'shutup',
    'شٹاپ',
    'شٹ اپ',
    'کت',
    'fuck',
    'lund',
]
TWITTER_HANDLES = [
    'HamidMirGEO',
    'CMShehbaz',
    'AftabIqbal786',
    'jasmeenmanzoor',
    'muneebfaruq',
    'ZarrarKhuhro',
    'ZaraHatKay_Dawn',
    'realm_zubair',
    'MaryamNSharif',
    'TalatHussain12',
    'betterpakistan',
    'MurtazaGeoNews',
    'AQpk',
    'KhawajaMAsif',
    'najamsethi',
    'asmashirazi',
    'meherbokhari',
    'Babar_Sattar',
    'KlasraRauf',
    'Xadeejournalist',
    'ejazhaider',
    '_Mansoor_Ali',
    'Fahdhusain',
    'Shahidmasooddr',
    'WaseemBadami',
    'BabarAwanPK',
    'NadeemfParacha',
]
INITIALS = [
    'Hey',
    'Hello',
    'Dear',
    '.',
    'hi-ya',
    'greetings',
    'Salam',
    'hola',
]
ACK = [
    'see how',
    'check how',
    'note how',
    'spot how',
]
BLAME = [
    'systematically',
    'aggressively',
    'constantly',
    '',
]
STYLE = [
    'abusing',
    'vilifying',
    'bad-mouthing',
]
PERSON = [
    '@{}'.format(CHAIRMAN_TWITTER),
    'Imran Khan',
    'IK',
    'Khan sahab',
    'Chairman PTI',
    'The Great Khan',
]
MARYAMS_NAMES = [
    'Raani',
    'Queen',
    'Shezadi',
    'Malka',
    'MaryamNS',
]
CONCLUSIONS = [
    'orchestrated',
    'approved',
    'ordered',
    'supported',
]

MSG = '{initials} @{handle}, {ack} PMLN SMT is {blame} {style} {person} - ' \
      '{name} {conclusion} ?'


def parse_response_and_tweet(raw_response):
    for tweet in raw_response['statuses']:
        tweet_text = tweet['text'].lower()
        if len(tweet_text) > 50:
            continue
        for word in CATCH_WORDS:
            if word in tweet_text:
                if not os.path.exists(FILE_PATH_COUNT):
                    write_record(FILE_PATH_COUNT, "0")
                count = int(read_record(FILE_PATH_COUNT))
                tweet_url = URL_TWEET.format(
                    screen_name=tweet['user']['screen_name'],
                    status_id=tweet['id']
                )
                tweet_text = '{} ({}) {}'.format(
                    get_random_message(), count, tweet_url
                )
                sleep(random.randrange(5))
                twitter_driver.tweet(tweet_text)
                count += 1
                write_record(FILE_PATH_COUNT, count)
                break


def get_random_message():
    return MSG.format(
        initials=random.choice(INITIALS),
        handle=random.choice(TWITTER_HANDLES),
        ack=random.choice(ACK),
        blame=random.choice(BLAME),
        style=random.choice(STYLE),
        person=random.choice(PERSON),
        name=random.choice(MARYAMS_NAMES),
        conclusion=random.choice(CONCLUSIONS),
    )


if __name__ == '__main__':
    config = ConfigHelpers(FILE_PATH_CREDENTIALS)
    twitter = Twitter(
        config.get_credential('access_token'),
        config.get_credential('access_token_secret'),
        config.get_credential('consumer_key'),
        config.get_credential('consumer_key_secret')
    )
    twitter_driver = TwitterDriver(config.get_credential('chrome_driver_path'))
    twitter_driver.login(
        config.get_credential('username'),
        config.get_credential('password')
    )
    while True:
        response = twitter.get_replies(
            CHAIRMAN_TWITTER,
            read_record(FILE_PATH_RECORDS)
        )
        write_record(FILE_PATH_RECORDS, response['search_metadata']['max_id'])
        parse_response_and_tweet(response)
        sleep(150)
