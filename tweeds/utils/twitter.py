import json
from time import sleep

import oauth2
from selenium import webdriver

from tweeds.utils.constants import (
    URL_SEARCH_BASE,
    URL_TIMELINE_BASE,
    URL_TWEET_BASE
)
from tweeds.utils.helpers import URLBuilder

URL_TWITTER = 'https://www.twitter.com/login'
XPATH_FIELD_USERNAME = '//*[@id="page-container"]/div/div[1]/form/fieldset/div[1]/input'
XPATH_FIELD_PASSWORD = '//*[@id="page-container"]/div/div[1]/form/fieldset/div[2]/input'
XPATH_FIELD_TWEET_TEXT = '//*[@id="tweet-box-home-timeline"]'
XPATH_BUTTON_TWEET = '//*[@id="timeline"]/div[2]/div/form/div[2]/div[2]/button'


class Twitter:
    def __init__(self, access_token, access_token_secret, consumer_key,
                 consumer_key_secret):
        consumer = oauth2.Consumer(consumer_key, consumer_key_secret)
        token = oauth2.Token(access_token, access_token_secret)
        self.client = oauth2.Client(consumer, token)

    def _get(self, url):
        resp, content = self.client.request(url)
        return json.loads(content.decode())

    def _post(self, url, body):
        resp, content = self.client.request(url, method='POST', body=body)
        return json.loads(content.decode())

    def get_latest_tweets(self, screen_name, count=2):
        url = URLBuilder(URL_TIMELINE_BASE)
        url.append('trim_user', 'true')
        url.append('screen_name', screen_name)
        url.append('count', count)
        return self._get(url.build())

    def get_tweet_ids(self, screen_name, count=2):
        return [tweet['id'] for tweet in
                json.loads(self.get_latest_tweets(screen_name, count))]

    def get_replies(self, screen_name, since_id=None):
        url = URLBuilder(URL_SEARCH_BASE)
        url.append('q', 'to:{}'.format(screen_name))
        url.append('result_type', 'recent')
        url.append('count', '100')
        if since_id:
            url.append('since_id', since_id)
        return self._get(url.build())

    def tweet(self, text):
        self._post(URL_TWEET_BASE, 'status={}'.format('{}'.format(text)))


class TwitterDriver(webdriver.Chrome):
    def __init__(self, executable_path, **kwargs):
        super().__init__(executable_path, **kwargs)
        self.get(URL_TWITTER)

    def login(self, username, password):
        username_field = self.find_element_by_xpath(XPATH_FIELD_USERNAME)
        password_field = self.find_element_by_xpath(XPATH_FIELD_PASSWORD)
        username_field.send_keys(username)
        sleep(1)
        password_field.send_keys(password)
        password_field.submit()

    def tweet(self, text):
        tweet_text_field = self.find_element_by_xpath(XPATH_FIELD_TWEET_TEXT)
        tweet_text_field.send_keys(text)
        sleep(1)
        tweet_button = self.find_element_by_xpath(XPATH_BUTTON_TWEET)
        tweet_button.click()
        sleep(5)
