import os

FILE_PATH_CREDENTIALS = os.path.expanduser('~/.tweeds_credentials.ini')
FILE_PATH_COUNT = os.path.expanduser('~/.config/tweeds_count.ini')
FILE_PATH_RECORDS = os.path.expanduser('~/.config/tweeds.ini')

URL_BASE = 'https://api.twitter.com/1.1/'
URL_TIMELINE_BASE = '{}{}'.format(URL_BASE, 'statuses/user_timeline.json?')
URL_SEARCH_BASE = '{}{}'.format(URL_BASE, 'search/tweets.json?')
URL_TWEET_BASE = '{}{}'.format(URL_BASE, 'statuses/update.json')
URL_TWEET = 'http://twitter.com/{screen_name}/status/{status_id}'
