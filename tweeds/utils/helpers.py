import configparser
import os

CONFIG_SECTION_CREDENTIALS = 'twitter'


class ConfigHelpers:
    def __init__(self, config_file):
        if not os.path.isfile(config_file):
            raise RuntimeError('Config file does not exist.')
        self.config = configparser.ConfigParser()
        self.config.read(config_file)

    def _read_config_parameter(self, config_section, config_key):
        try:
            return self.config.get(config_section, config_key)
        except configparser.NoOptionError or configparser.NoSectionError:
            return None

    def get_credential(self, key):
        return self._read_config_parameter(CONFIG_SECTION_CREDENTIALS, key)


class URLBuilder:
    _url = str()

    def __init__(self, base_url):
        self._url = base_url

    def append(self, key, value):
        self._url += '{}={}&'.format(key, value)
        return self

    def build(self):
        return self._url[:-1]


def read_record(file_path):
    record = str()
    if os.path.exists(file_path):
        with open(file_path, 'r') as file:
            record = file.read().strip()
    return record


def write_record(file_path, record):
    with open(file_path, 'w') as file:
        file.write(str(record))
